import numpy as np
import orekit
orekit.initVM()

from orekit.pyhelpers import setup_orekit_curdir
setup_orekit_curdir()

from org.orekit.time import AbsoluteDate, TimeScalesFactory

if __name__=="__main__":

    utc = TimeScalesFactory.getUTC()
    tai = TimeScalesFactory.getTAI()

    start = AbsoluteDate(2005, 12, 31, 23, 59, 50.00, utc)
    duration = 20.0
    end = start.shiftedBy(duration)
    print("          UTC                    TAI date")

    step = 0.5

    current = start
    for i in np.arange(0, duration, step):
        d = current.shiftedBy(step)
        print(f"{d.toString(utc)}        {d.toString(tai)}")
        current = d


